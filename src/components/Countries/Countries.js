import React, {Component} from 'react';
import axios from 'axios';

import './Countries.css'

axios.defaults.baseURL = 'https://restcountries.eu/rest/v2';

class Countries extends Component {

  state = {
    countries: [],
    currentCountry: {},
    currentCountryBorders: []
  };

  componentDidMount() {
    this.getCountries()
      .then(this.getCountriesInfo)
      .then(this.writeCountriesToState)
      .catch(error => {
      console.log(error);
    });
  }

  getCountries = () => {
    return axios.get('/all?fields=name;alpha3Code')
  };

  getCountriesInfo = response => {
    const requests = response.data.map(country => {
      return axios.get(`alpha/${country.alpha3Code}`);
    });

    return Promise.all(requests);
  };

  writeCountriesToState = response => {
    const currentCountry = response[0].data;
    this.getBordersInfo(currentCountry)
      .then(borders => {
        this.setState({countries: response.map(c => c.data),
          currentCountry,
          currentCountryBorders: borders.map(c => {return c.data.name})});
      });
  };

  getBordersInfo = country => {
    const requests = country.borders.map(code => {
      return axios.get(`alpha/${code}`);
    });

    return Promise.all(requests);
  };

  selectCountry = event => {
    const currentCountry = this.state.countries.find(c => c.alpha3Code === event.target.id);
    this.getBordersInfo(currentCountry)
      .then(response => {
        this.setState({currentCountry, currentCountryBorders: response.map(c => {return c.data.name})});
      })
  };


  render() {
    const countriesList = this.state.countries.map(country => {
      let className = 'countryList-item';
      if (country === this.state.currentCountry) {
        className += ' countryList-item--active';
      }
      return <div className={className}
                  key={country.alpha3Code}
                  id={country.alpha3Code}
                  onClick={this.selectCountry}>
        {country.name}</div>
    });

    return (
      <div className="Countries">
        <div className="countriesList">{countriesList}</div>
        <div className="countryInfo">
          <div><b>Name: </b> {this.state.currentCountry.name}</div>
          <div><b>Capitol: </b> {this.state.currentCountry.capital}</div>
          <div><b>Subregion: </b> {this.state.currentCountry.subregion}</div>
          <div><b>Border with: {this.state.currentCountryBorders.join(', ')}</b></div>
        </div>
      </div>
    );
  }
}

export default Countries;